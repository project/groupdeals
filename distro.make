; Use this file to build a full distribution including Drupal core and the
; Groupdeals install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"

; Add Groupdeals to the full distribution build.
projects[groupdeals][type] = profile
projects[groupdeals][version] = 1.x-dev
projects[groupdeals][download][type] = git
projects[groupdeals][download][url] = http://git.drupal.org/project/groupdeals.git
projects[groupdeals][download][branch] = 7.x-1.x